#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <execinfo.h>
#include <limits.h>
#include <jemalloc/jemalloc.h>
#include "uthash.h"

#define KB(n) n << 10
#define MB(n) n << 20
#define GB(n) n << 30
#define MAXSTR 512
#define FULL_CXT_SIZE 10
#define UNUSED_CXT 2
#define CXT_SIZE (FULL_CXT_SIZE - UNUSED_CXT)
#define MAX_CXT_STRLEN (CXT_SIZE*MAXSTR)
#define INVALID_ARENA UINT_MAX
#define MY_MALLOCX_ARENA(a) (((int)(a)+1) <<20)
unsigned hot_idx  = 0;
unsigned cold_idx = 0;

int marena_active = 0;
int malloc_cnt = 0;
int top_cxt_id = 0;
static int cnt = 0;
int disable_lookup = 0;
int per_phase_hotvecs = 0;

extern void *__libc_malloc(size_t size);
extern void *__libc_calloc(size_t nmemb, size_t size);
extern void *__libc_realloc(void *ptr, size_t size);
extern void __libc_free(void *ptr);

void *marena_malloc(size_t size, unsigned arena_idx);
void *marena_calloc(size_t nmemb, size_t size, unsigned arena_idx);
void *marena_realloc(void *ptr, size_t size, unsigned arena_idx);
void marena_free(void *ptr);

int __attribute__ ((constructor)) marena_init(void);
int __attribute__ ((destructor))  marena_exit(void);

FILE *malloc_stats_fp;

struct alloc_point_info {
    void *key[CXT_SIZE];
    char cxt_strings[MAX_CXT_STRLEN];
    int id;
    /*int hot;*/
    unsigned arena_idx;
    unsigned hot_phases;
    UT_hash_handle hh;
};

struct alloc_point_info *alloc_point_hash = NULL;

struct hot_ap_entry {
    char cxt_strings[MAX_CXT_STRLEN];
    UT_hash_handle hh;
    unsigned arena_idx;
    unsigned hot_phases;
};

struct hot_ap_entry *hot_aps = NULL;

struct ptr_key {
  void *key;
  size_t size;
  struct alloc_point_info *ap_info;
  UT_hash_handle hh;
};

struct ptr_key *ptr_records = NULL;

struct hotvec_info {
  unsigned key;
  unsigned aid;
  UT_hash_handle hh;
};

struct hotvec_info *hotvec_arena_hash = NULL;

struct alloc_point_info *get_alloc_point_info()
{
    void *tmp_buffer[FULL_CXT_SIZE], *buffer[CXT_SIZE];
    int i, tmp_nptrs, nptrs, cxt_size, pos;
    struct alloc_point_info *ap_info;
    struct hot_ap_entry *hot_entry;
    char **tmp_strings, *tmp;

    /* APH - Disabled by -n in the harness for overhead measurement */
    if (disable_lookup) {
        return NULL;
    }

    /* put the backtrace in the tmp_buffer -- the first few items of context
     * are always from the libmarena library, so just discard them
     */
    tmp_nptrs = backtrace(tmp_buffer, FULL_CXT_SIZE);
    if (tmp_nptrs < FULL_CXT_SIZE) {
        for (i = tmp_nptrs; i < FULL_CXT_SIZE; i++) {
            tmp_buffer[i] = NULL;
        }
    }

    if (tmp_nptrs < 3) {
        printf("error: backtrace assumption violated\n");
        exit(EXIT_FAILURE);
    }

    nptrs = (tmp_nptrs - UNUSED_CXT);
    for (i = 0; i < CXT_SIZE; i++) {
        buffer[i] = tmp_buffer[i+UNUSED_CXT];
    }

    HASH_FIND(hh, alloc_point_hash, &(buffer[0]), (CXT_SIZE*sizeof(void*)), ap_info);

    if (ap_info == NULL) {

        ap_info = malloc(sizeof(struct alloc_point_info));
        if (ap_info == NULL) {
            printf("error: could not allocate alloc_point_info\n");
            exit(ENOMEM);
        }

        memcpy(&(ap_info->key[0]), &(buffer[0]), (CXT_SIZE*sizeof(void*)));

        tmp_strings = backtrace_symbols(buffer, nptrs);
        if (tmp_strings == NULL) {
            perror("backtrace_symbols");
            exit(EXIT_FAILURE);
        }

#if 0
        /* MRJ -- check if the context length is too large -- uncomment this
         * if one of the benchmarks fails unexpectedly
         */
        cxt_size = 0;
        for (i = 0; i < nptrs; i++) {
            char *token, *saveptr;

            token = strtok_r(tmp_strings[i], " ", &saveptr);
            if (token == NULL) {
                perror("tokenizing tmp_strings");
                exit(EXIT_FAILURE);
            }

            cxt_size += (strlen(token)+1);
        }
        cxt_size += 1;

        if (cxt_size > MAX_CXT_STRLEN) {
            printf("context too large\n");
            exit(EXIT_FAILURE);
        }
#endif

        pos = 0;
        for (i = 0; i < nptrs; i++) {
            char *token, *saveptr;

            token = strtok_r(tmp_strings[i], " ", &saveptr);
            if (token == NULL) {
                perror("tokenizing tmp_strings");
                exit(EXIT_FAILURE);
            }

            strcpy(&(ap_info->cxt_strings[pos]), token);
            pos += (strlen(token)+1);
        }
        memset(&(ap_info->cxt_strings[pos]), '\0', (MAX_CXT_STRLEN-pos+1));
        free(tmp_strings);

        ap_info->id = top_cxt_id;
        top_cxt_id++;

        /* find it in the hot_aps hash */
        HASH_FIND(hh, hot_aps, &(ap_info->cxt_strings[0]), MAX_CXT_STRLEN, hot_entry);
        if (hot_entry != NULL) {
            ap_info->arena_idx = hot_entry->arena_idx;
            ap_info->hot_phases = hot_entry->hot_phases;
        } else {
            ap_info->arena_idx = cold_idx;
            ap_info->hot_phases = 0;
        }

        HASH_ADD(hh, alloc_point_hash, key, (CXT_SIZE*sizeof(void*)), ap_info);
    }

    return ap_info;
}

/* get_arena_idx: given an alloc_point_info structure, return the arena to
 * allocate new objects from. If ap_info is NULL, return the hot_idx
 */
unsigned get_arena_idx(struct alloc_point_info *ap_info) {
    if (!ap_info) {
        return hot_idx;
    } 
    return ap_info->arena_idx;
}

void marena_bound(void *ptr) {
#if 0
    fprintf(stderr, "marena_bound: %lu %lu\n", (((intptr_t)ptr)>>12), ptr);
#endif
}

/* MRJ -- The marena_bound function communicates to Pin the expected bounds of
 * each arena. There is certainly a better way to do this. Consider using the
 * end symbol available with etext
 */
void do_marena_bound(unsigned aid) {
    int err;
    void *ptr;

    ptr = mallocx(sizeof(int), MALLOCX_ARENA(aid));
    /*
    if ( (ptr = mallocx(sizeof(int), MALLOCX_ARENA(aid))) == NULL ) {
        fprintf(stderr, "Error allocm failed: %d\n", err);
        fflush(stderr);
        errno = ENOMEM;
        return;
    }
*/
    marena_bound(ptr);

    dallocx(ptr, 0);
        //fprintf(stderr, "Error dallocx failed: %d\n", err);
    
}

void xps_malloc_log(size_t size, void *ptr, int idx, struct alloc_point_info *ap_info) {
#if 0
    int pos;
    fprintf(stderr, "xps_malloc: %lu %lu %zu %d (CXT ID: %d)\n", (((intptr_t)ptr)>>12),
           ptr, size, get_arena_idx(ap_info), ap_info->id);

    pos = 0;
    while ((ap_info->cxt_strings[pos]) != '\0') {
        fprintf(stderr, "  %s\n", &(ap_info->cxt_strings[pos]));
        pos += (strlen(&(ap_info->cxt_strings[pos]))+1); 
    }
    fprintf(stderr,"\n");
#endif
}
void xps_calloc_log(size_t nmemb, size_t size, void *ptr, int idx, struct alloc_point_info *ap_info) {
#if 0
    int pos;

    fprintf(stderr, "xps_calloc: %lu %lu %zu %d (CXT ID: %d)\n", (((intptr_t)ptr)>>12),
            ptr, (nmemb*size), get_arena_idx(ap_info), ap_info->id);

    pos = 0;
    while ((ap_info->cxt_strings[pos]) != '\0') {
        //printf("pre  pos: %d ap_info: %p, str: %p\n", pos, ap_info, &(ap_info->cxt_strings[pos]));
        fprintf(stderr,"  %s\n", &(ap_info->cxt_strings[pos]));
        pos += (strlen(&(ap_info->cxt_strings[pos]))+1); 
        //printf("post pos: %d ap_info: %p, str: %p\n", pos, ap_info, &(ap_info->cxt_strings[pos]));
    }
    fprintf(stderr,"\n");
#endif
}
void xps_realloc_log(void *ptr, size_t size, void *rptr, int idx, struct alloc_point_info *ap_info) {
#if 0
    int pos;

    fprintf(stderr, "xps_realloc: %lu %lu %zu %lu %lu %d (CXT ID: %d)\n",
            (((intptr_t)ptr)>>12), ptr, size, (((intptr_t)rptr)>>12), rptr,
            get_arena_idx(ap_info), ap_info->id);

    pos = 0;
    while ((ap_info->cxt_strings[pos]) != '\0') {
        fprintf(stderr, "  %s\n", &(ap_info->cxt_strings[pos]));
        pos += (strlen(&(ap_info->cxt_strings[pos]))+1); 
    }
    fprintf(stderr,"\n");
#endif
}
void xps_free_log(void *ptr) {
#if 0
    fprintf(stderr, "xps_free: %lu %lu\n", (((intptr_t)ptr)>>12), ptr);
#endif
}

void add_to_ptr_records(void *ptr, size_t size, struct alloc_point_info *ap_info) {
    struct ptr_key *pk = NULL;

    HASH_FIND_PTR(ptr_records, &ptr, pk);
    if (pk != NULL) {
        /* there is already a record so return */
        return;
    }

    pk = (struct ptr_key*)malloc(sizeof(struct ptr_key));
    if (pk == NULL) {
        exit(ENOMEM);
    }

    pk->key = ptr;
    pk->size = size;
    pk->ap_info = ap_info;
    HASH_ADD_PTR(ptr_records, key, pk);
}

void *marena_malloc(size_t size, unsigned arena_idx) {
    int err;
    void *ptr;

    if (size == 0) {
        return NULL;
    }

    if ( (ptr = mallocx(size, MALLOCX_ARENA(arena_idx))) == NULL ) {
        fprintf(stderr, "Error allocm failed: %d\n", err);
        errno = ENOMEM;
        return NULL;
    }

    return ptr;
}

void *marena_calloc(size_t nmemb, size_t size, unsigned arena_idx) {
    int err;
    size_t my_size;
    void *ptr;

    if (nmemb == 0 || size == 0) {
        return NULL;
    }

    my_size = (nmemb*size);
    if ( (ptr = mallocx(my_size, MALLOCX_ARENA(arena_idx))) == NULL) {
        fprintf(stderr, "Error callocm failed: %d\n", err);
        errno = ENOMEM;
        return NULL;
    }
    /* doing these accesses before doing xps_calloc_log will put the pages on
     * the wrong tier
     */
    //memset(ptr, 0, my_size);

    return ptr;
}

void *marena_realloc(void *ptr, size_t size, unsigned arena_idx) {
    int err;

    /* earlier checks ensure ptr is non-null and size is non-zero */
    if ( (ptr = rallocx(ptr, size, MALLOCX_ARENA(arena_idx))) == NULL ) {
        fprintf(stderr, "Error rallocm failed: %d\n", err);
        errno = ENOMEM;
        return NULL;
    }
    return ptr;
}

void marena_free(void *ptr) {
    int err;
    if (ptr) {
      dallocx(ptr, 0);
            //fprintf(stderr, "Error dallocm failed: %d\n", err);
      
    }
}

void *malloc(size_t size) {
    if (marena_active) {
        void *ptr;
        struct alloc_point_info *ap_info;
#if 0
        marena_active = 0;
        fprintf(stderr, "enter: cnt: %14d amalloc: %zu\n", cnt, size); fflush(stderr);
        marena_active = 1;
#endif

        marena_active = 0;

        ap_info = get_alloc_point_info();
#if 0
        fprintf(stderr, "cnt: %14d amalloc: %zu %d --> ", cnt, size, get_arena_idx(ap_info)); fflush(stderr);
#endif
        ptr = marena_malloc(size, get_arena_idx(ap_info));
        if (ptr != NULL) {
            add_to_ptr_records(ptr, size, ap_info);
            xps_malloc_log(size, ptr, get_arena_idx(ap_info), ap_info);
        }
#if 0
        fprintf(stderr, "%lu\n", ptr); fflush(stderr); cnt++;
#endif
        
        marena_active = 1;

        return ptr;

    }
    return __libc_malloc(size);
#if 0
    void *ptr;
    ptr = __libc_malloc(size);
    if (marena_active) {
        marena_active = 0;
        fprintf(stdout, "cnt: %14d malloc:  %zu --> %p\n", cnt, size, ptr); fflush(stdout);
        cnt++;
        marena_active = 1;
    }
    return ptr;
#endif
}

void *calloc(size_t nmemb, size_t size) {
    if (marena_active) {
        void *ptr;
        struct alloc_point_info *ap_info;
#if 0
        marena_active = 0;
        fprintf(stderr, "enter: cnt: %14d acalloc: %zu\n", cnt, size); fflush(stderr);
        marena_active = 1;
#endif

        marena_active = 0;

        ap_info = get_alloc_point_info();
#if 0
        fprintf(stderr, "cnt: %14d acalloc: %zu %zu %d --> ", cnt, nmemb, size, get_arena_idx(ap_info)); fflush(stderr);
#endif
        ptr = marena_calloc(nmemb, size, get_arena_idx(ap_info));
        if (ptr != NULL) {
            add_to_ptr_records(ptr, (nmemb*size), ap_info);
            xps_calloc_log(nmemb, size, ptr, get_arena_idx(ap_info), ap_info);
            memset(ptr, 0, (nmemb*size));
        }
#if 0
        fprintf(stderr, "%lu\n", ptr); fflush(stderr); cnt++;
#endif
        
        marena_active = 1;

        return ptr;
    }
    return __libc_calloc(nmemb, size);
#if 0
    void *ptr;
    ptr = __libc_calloc(nmemb, size);
    if (marena_active) {
        marena_active = 0;
        fprintf(stdout, "cnt: %14d calloc:  %zu %zu --> %p\n", cnt, nmemb, size, ptr); fflush(stdout);
        cnt++;
        marena_active = 1;
    }
    return ptr;
#endif
}

void *realloc(void *ptr, size_t size) {
    if (marena_active) {
        void *rptr;
        struct alloc_point_info *ap_info;
        struct ptr_key *pk;
#if 0
        marena_active = 0;
        fprintf(stderr, "enter: cnt: %14d arealloc: %p %zu\n", cnt, ptr, size); fflush(stderr);
        marena_active = 1;
#endif

        if (ptr == NULL) {
            return malloc(size);
        }

        if (size == 0) {
            free(ptr);
            return NULL;
        }

        pk = NULL;
        HASH_FIND_PTR(ptr_records, &ptr, pk);
        if (pk == NULL) {
            //printf("warning: realloc on address that was never malloc'ed: %p\n", ptr);
            return malloc(size);
        }

        marena_active = 0;

        ap_info = get_alloc_point_info();

        /* It is possible the previous allocation of this data is not in the
         * same arena as the realloc. In these cases, simply do a malloc in
         * the correct arena instead of a realloc
         */
        if (get_arena_idx(pk->ap_info) != get_arena_idx(ap_info)) {
#if 0
            fprintf(stderr, "mis: %14d arealloc: %lu %zu %zu %d %d --> ",
                    cnt, ptr, pk->size, size, get_arena_idx(pk->ap_info),
                    get_arena_idx(ap_info)); fflush(stderr);
#endif

            ptr = marena_malloc(size, get_arena_idx(ap_info));
            if (ptr != NULL) {
                xps_malloc_log(size, ptr, get_arena_idx(ap_info), ap_info);
                memcpy(ptr, pk->key, ((size < pk->size) ? size : pk->size));
                add_to_ptr_records(ptr, size, ap_info);

                marena_free(pk->key);
                xps_free_log(pk->key);
                HASH_DEL(ptr_records, pk);
                free(pk);
                
                //xps_malloc_log(size, ptr, get_arena_idx(ap_info), ap_info);
#if 0
                fprintf(stderr, "%lu\n", ptr); fflush(stderr); cnt++;
#endif
            }

            marena_active = 1;
            return ptr;
        }
#if 0
        fprintf(stderr, "cnt: %14d arealloc: %lu %zu %d --> ", cnt, ptr, size,
                get_arena_idx(ap_info)); fflush(stderr);
#endif
        rptr = marena_realloc(ptr, size, get_arena_idx(ap_info));
        if (rptr != NULL) {
            if (rptr != ptr) {
                struct ptr_key *pk = NULL;

                HASH_FIND_PTR(ptr_records, &ptr, pk);
                if (pk != NULL) {
                    HASH_DEL(ptr_records, pk);
                    free(pk);
                }

                /* add logic to free ptr because the data moved */
                add_to_ptr_records(rptr, size, ap_info);
            } else {
                HASH_FIND_PTR(ptr_records, &ptr, pk);
                pk->size = size;
            }
            xps_realloc_log(ptr, size, rptr, get_arena_idx(ap_info), ap_info);
        }
#if 0
        fprintf(stderr, "%lu\n", rptr); fflush(stderr); cnt++;
#endif
        
        marena_active = 1;

        return rptr;
    }
    return __libc_realloc(ptr, size);
#if 0
    void *rptr;
    rptr = __libc_realloc(ptr, size);
    if (marena_active) { 
        marena_active = 0;
        fprintf(stdout, "cnt: %14d realloc:  %p %zu --> %p\n", cnt, ptr, size, rptr); fflush(stdout);
        cnt++;
        marena_active = 1;
    }
    return rptr;
#endif
}

void free(void *ptr) {
    if (marena_active) {
        struct ptr_key *pk;

        marena_active = 0;

#if 0
        fprintf(stderr, "cnt: %14d afree: %lu\n", cnt, ptr); fflush(stderr); cnt++;
#endif
        pk = NULL;
        HASH_FIND_PTR(ptr_records, &ptr, pk);
        if (pk == NULL) {
            free(ptr);
            marena_active = 1;
            return;
        }
        HASH_DEL(ptr_records, pk);
        free(pk);

        marena_free(ptr);
        xps_free_log(ptr);

        marena_active = 1;

        return;
    }
#if 0
    if (marena_active) {
        marena_active = 0;
        fprintf(stdout, "cnt: %14d free:  %p\n", cnt, ptr); fflush(stdout);
        cnt++;
        marena_active = 1;
    }
#endif
    __libc_free(ptr);
}

void load_hot_aps(const char *hot_aps_file) {

    FILE *hotfp;
    int i, err, pos;
    unsigned mask, aid;
    struct hot_ap_entry *hape;
    struct hotvec_info *hvi;
    char buf[MAXSTR], *tmp;
    size_t size;
    unsigned int arena_cnt = 0;

    size = sizeof(unsigned);

    hotfp = fopen(hot_aps_file, "r");
    if (hotfp == NULL) {
        printf("error: could not open hot_aps_file: %s", hot_aps_file);
    }

    while (fgets(buf, MAXSTR, hotfp) != NULL) {

        hape = malloc(sizeof(struct hot_ap_entry));
        if (hape == NULL) {
            printf("error: could not allocate hot_ap_entry\n");
            exit(ENOMEM);
        }
        
        if (per_phase_hotvecs) {

            /* In a run using phase-based profiling, we allocate a separate 
             * arena for each group of allocation sites who are all hot during 
             * the same set of phases. A mapping of grouping id to arena id is 
             * maintained for this purpose.
             */
            if (fgets(buf, MAXSTR, hotfp) == NULL || sscanf(buf, "%i", &mask) != 1) {
                printf("error: expected hot phase bit set for alloc point (set by -p)\n");
                exit(1);
            }

            HASH_FIND_INT(hotvec_arena_hash, &mask, hvi);
            if (hvi == NULL) {
                if ((err = mallctl("arenas.create", &aid, &size, NULL, 0)) != 0 ) {
                    fprintf(stderr, "Error making hot arena: %d\n", err);
                    exit(1);
                }

                do_marena_bound(aid);

                hvi = (struct hotvec_info*)malloc(sizeof(struct hotvec_info));
                if (hvi == NULL) {
                    exit(ENOMEM);
                }

                hvi->key = mask;
                hvi->aid = aid;
                HASH_ADD_INT(hotvec_arena_hash, key, hvi);

                arena_cnt++;
            }

            hape->arena_idx  = hvi->aid;
            hape->hot_phases = mask;

        } else {
            hape->arena_idx = hot_idx;
            hape->hot_phases = UINT_MAX;
        }

        pos = 0;
        while (fgets(buf, MAXSTR, hotfp) != NULL) {

            /* newline indicates we are done with this site */
            if (strcmp(buf, "\n") == 0) {
                break;
            }

            /* do not copy the new line into the cxt_strings buffer */
            if (buf[strlen(buf)-1] == '\n') {
                buf[strlen(buf)-1] = '\0';
            }

            strcpy(&(hape->cxt_strings[pos]), buf);
            pos += (strlen(buf)+1);
        }

        memset(&(hape->cxt_strings[pos]), '\0', (MAX_CXT_STRLEN-pos+1));
        HASH_ADD(hh, hot_aps, cxt_strings, (MAX_CXT_STRLEN*sizeof(char)), hape);
    }

    fclose(hotfp);
}

int marena_init() {
    size_t size, lg_chunk;
    int err, ppid, i;
    char *nolookup, *hot_aps_file, *groupaps;
    FILE *fp;
    nolookup = getenv("MARENA_DISABLE_LOOKUP");
    if (nolookup != NULL && strcmp(nolookup, "True") == 0) {
        disable_lookup = 1;
    }
   
    groupaps = getenv("MARENA_GROUP_ALLOC_POINTS");
    if (groupaps != NULL && strcmp(groupaps, "True") == 0) {
        per_phase_hotvecs = 1;
    }

    size = sizeof(unsigned);
    if ((err = mallctl("arenas.create", &cold_idx, &size, NULL, 0)) != 0 ) {
        fprintf(stderr, "Error making cold arena: %d\n", err);
        return err;
    }
    do_marena_bound(cold_idx);

    if (!per_phase_hotvecs) {
        size = sizeof(unsigned);
        if ((err = mallctl("arenas.create", &hot_idx, &size, NULL, 0)) != 0 ) {
            fprintf(stderr, "Error making hot arena: %d\n", err);
            return err;
        }
        do_marena_bound(hot_idx);
    }
     
    hot_aps_file = getenv("HOT_APS_FILE");
    if (hot_aps_file != NULL) {
        load_hot_aps(hot_aps_file);
    }
   
    marena_active = 1;
    malloc_cnt = 0;
    top_cxt_id = 0;

    return 0;
}

void print_malloc_stats(void *cbopaque, const char *s) {
    fwrite(s, strlen(s), 1, malloc_stats_fp);
}

int marena_exit() {
    char *malloc_stats_file;
    int err;

    malloc_stats_file = getenv("MALLOC_STATS_FILE");
    if (malloc_stats_file != NULL) {

        malloc_stats_fp = fopen(malloc_stats_file, "w");

        if (malloc_stats_fp == NULL) {
            fprintf(stderr, "Error opening malloc_stats_file: %d\n", err);
            return err;
        }

        malloc_stats_print(print_malloc_stats,NULL,NULL);
        fclose(malloc_stats_fp);
    }
}
