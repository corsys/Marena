CC=gcc
CFLAGS=-Wall -O2 -I/projects/zephyr/teffler/xps/jemalloc/include/
LDFLAGS= -ldl -L/projects/zephyr/teffler/xps/jemalloc/obj/lib
SOURCES=marena.c
OBJECTS=marena.o
MARENA=marena

LIBCFLAGS=-Wall -O2 -I/projects/zephyr/teffler/xps/jemalloc/include
LIBLDFLAGS=-fPIC -shared -ldl -L/projects/zephyr/teffler/xps/jemalloc/obj/lib -ldl -ljemalloc
LIBSOURCES=libmarena.c
LIBOBJECTS=libmarena.o
LIBMARENA=libmarena.so

all: $(LIBMARENA) $(MARENA)

$(LIBMARENA): $(LIBSOURCES)
	$(CC) $(LIBCFLAGS) $(LIBLDFLAGS) $< -o $@

$(MARENA): $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) $< -o $@

$(OBJECTS): $(SOURCES)
	$(CC) $(LDFLAGS) $(CFLAGS) -c $<

clean:
	rm -f *.o $(LIBMARENA) $(MARENA)
